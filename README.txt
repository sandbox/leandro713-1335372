Send comments, ideas, feedback, etc to leandro@leandro.org
This software is under GPLv3 license.

INSTALLATION
============

This module allows you to show a block in Drupal 7 with 11870 sites near the web visitor. You can specify a topic (restaurant, museum, shop, etc..)  and a maximum radius to search.
This is the correct order to do it works:
1. upload the directory «onceocho» to your modules directory
2. enable the module
3. configure the module settings (otherwise the block gives no output)
4. place it in some area of your theme (blocks admin drupal section)

Thats all!

CONFIGURATION
=============

In order to do this block work you will need the following data and files:
1. download maxmind geolocation dat file from 
http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
(GPL license, 30 MB when uncompressed)
remember where you put it in your system, so this block will ask you where it is
2. grab a Google Maps user key for your site if you dont have it already
3. grab token & secret user keys from 11870 API
4. create somewhere a cache directory, this block uses SimplePie and it requires a cache directory. This block will ask you where it is.

